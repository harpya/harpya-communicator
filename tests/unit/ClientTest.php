<?php
/**
 * Created by PhpStorm.
 * User: eduardoluz
 * Date: 2019-04-03
 * Time: 10:47 PM
 */

namespace unit;

use harpya\communicator\Client;

class ClientTest extends \PHPUnit\Framework\TestCase
{
    public function testInstantiateClient()
    {
        $client = new Client();

        $this->assertEquals(getenv(Client::DISCOVER_URL), $client->getDiscoverURL());
        $this->assertEquals(getenv(Client::DISCOVER_LOCAL_MODE), $client->getMode());
    }
}
