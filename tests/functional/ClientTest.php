<?php

namespace functional;

use GuzzleHttp\Exception\ClientException;
use harpya\communicator\bo\ServiceBO;
use harpya\communicator\Client;
use harpya\communicator\Service;

/**
 * Class ClientTest
 * @package functional
 *
 * TODO initialize the DB with just a couple services.
 */
class ClientTest extends \PHPUnit\Framework\TestCase
{
    const SERVICE_NAME = 'myTestCreateNewService';
    protected $port = 80;

    public function testPrevious()
    {
        if (!getenv(Client::DISCOVER_URL)) {
            // Ignoring, since there is no Discover instance configured
            echo "\n To perform the functional tests, is necessary have a valid Discovery instance configured\n";
            exit;
        }

        $this->assertTrue(true);
    }

    public function testConnectWithDiscover()
    {
        $client = new Client();
        $client->init(); // loads the Discover directory

        $services = $client->getServicesList();
        $this->assertTrue(is_array($services));
    }

    public function testCreateNewService()
    {
        $this->port = rand(1000, 30000);

        $serviceSpec = $this->getServiceSpecification();

        $client = new Client();
        $client->init();
        $response = $client->createService($serviceSpec);

        $this->assertTrue(is_array($response));
        $this->assertTrue($response['success']);
    }

    protected function getServiceSpecification() {
        $serviceSpec = [
            'name' => self::SERVICE_NAME,
            'version' => 1,
            'host' => 'http://127.0.0.1',
            'port' => $this->port
        ];
        return $serviceSpec;
    }

    /**
     * @depends testCreateNewService
     */
    public function testCreateServiceDuplicated()
    {
        $serviceSpec = $this->getServiceSpecification();

        $client = new Client();
        $client->init();
        $client->createService($serviceSpec);

        $response = $client->createService($serviceSpec);
//        print_r($response);
        $this->assertTrue(is_array($response));
        $this->assertFalse($response['success']);
        $this->assertTrue(isset($response['code']));
        $this->assertEquals(1002, $response['code']);
    }

    public function testClientRequestWithoutAuth()
    {
        $client = new Client([Client::DISCOVER_AUTH_TOKEN => false]);
        $client->init();
        $this->expectException('harpya\communicator\exception\ClientException');
        $client->getService(self::SERVICE_NAME);
    }

    public function testGetService()
    {
        $client = new Client();
        $client->init();
        $service = $client->getService(self::SERVICE_NAME);
        $this->assertTrue(is_object($service));
        $this->assertTrue((get_class($service) == ServiceBO::class));
        $this->assertEquals(self::SERVICE_NAME, $service->getName());
    }

    public function testGetServiceVersionInexistent()
    {
        $client = new Client();
        $client->init();
        $this->expectException('harpya\communicator\exception\ClientException');
        $service = $client->getService(self::SERVICE_NAME, 10);
    }

    public function testGetInexistentService()
    {
        $client = new Client();
        $this->expectException('harpya\communicator\exception\ClientException');
        $client->getService('inexistent');
    }
}
