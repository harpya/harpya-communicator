<?php

namespace harpya\communicator;

/**
 * Class BaseCommunicator
 * @author Eduardo Luz
 * @package harpya\communicator
 */
abstract class BaseCommunicator
{
    protected $props = [];

    const DISCOVER_URL = 'HARPYA_DISCOVER_URL';
    const DISCOVER_AUTH_TOKEN = 'HARPYA_DISCOVER_AUTH_TOKEN';
    const DISCOVER_LOCAL_MODE = 'HARPYA_DISCOVER_LOCAL_MODE';
    const MODE_CLIENT = 'client';
    const MODE_SERVICE = 'service';

    /**
     * @return mixed
     */
    abstract public function getMode();

    /**
     * BaseCommunicator constructor.
     * @param array $props
     */
    public function __construct($props = [])
    {
        $this->loadEnvConfig();

        if (is_array($props) && !empty($props)) {
            $this->props = array_merge($this->props, $props);
        }
    }

    /**
     * Just load the props with $_ENV values
     */
    protected function loadEnvConfig()
    {
        $props = $_ENV;
        if (is_array($props)) {
            $this->props = $props;
        }
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return bool|mixed
     */
    protected function getProp($name, $default = false)
    {
        if (isset($this->props[$name])) {
            return $this->props[$name];
        } else {
            return $default;
        }
    }

    /**
     *
     * @return string
     */
    public function getDiscoverURL()
    {
        return $this->getProp(self::DISCOVER_URL);
    }
}
