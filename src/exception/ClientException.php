<?php
/**
 * Created by PhpStorm.
 * User: eduardoluz
 * Date: 2019-04-09
 * Time: 10:09 PM
 */

namespace harpya\communicator\exception;

class ClientException extends \Exception
{
    protected $statusCode;

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     * @return ClientException
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }
}
