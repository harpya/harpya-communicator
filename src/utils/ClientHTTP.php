<?php

namespace harpya\communicator\utils;

/**
 * Class ClientHTTP
 * @package harpya\communicator\utils
 */
class ClientHTTP extends \GuzzleHttp\Client
{
    /**
     * @param $method
     * @param string $uri
     * @param array $options
     * @return mixed|ResponseInterface
     */
    public function requestRaw($method, $uri = '', array $options = [])
    {
        $response = parent::request($method, $uri, $options);
        $text = $response->getBody()->getContents();

        $arr = json_decode($text, true);
        return $arr;
    }
}
