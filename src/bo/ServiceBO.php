<?php
/**
 * Created by PhpStorm.
 * User: eduardoluz
 * Date: 2019-04-08
 * Time: 11:13 PM
 */

namespace harpya\communicator\bo;

class ServiceBO
{
    protected $key;
    protected $id;
    protected $name;
    protected $host;
    protected $port;
    protected $version;
    protected $healthCheck;

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     * @return ServiceBO
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Service
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Service
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     * @return Service
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param mixed $port
     * @return Service
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return Service
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHealthCheck()
    {
        return $this->healthCheck;
    }

    /**
     * @param mixed $healthCheck
     * @return Service
     */
    public function setHealthCheck($healthCheck)
    {
        $this->healthCheck = $healthCheck;
        return $this;
    }

    public function bind($arr)
    {
        if (isset($arr['key'])) {
            $this->setKey($arr['key']);
        }
        if (isset($arr['id'])) {
            $this->setId($arr['id']);
        }
        if (isset($arr['host'])) {
            $this->setHost($arr['host']);
        }
        if (isset($arr['port'])) {
            $this->setPort($arr['port']);
        }
        if (isset($arr['name'])) {
            $this->setName($arr['name']);
        }
    }
}
