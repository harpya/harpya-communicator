<?php

namespace harpya\communicator;

use harpya\communicator\bo\ServiceBO;
use harpya\communicator\exception\ClientException;
use harpya\communicator\utils\ClientHTTP;

/**
 * Class Client
 * @package harpya\communicator
 */
class Client extends BaseCommunicator
{
    const REQUEST_GET_SERVICE = 'get_service';

    protected $clientHTTP;

    /**
     * @return string
     */
    public function getMode()
    {
        return self::MODE_CLIENT;
    }

    /**
     * Make connection with Discovery instance to retrieve the directory
     *
     * TODO Use a local cache to improve performance
     */
    public function init()
    {
        //
    }

    /**
     * @return array
     */
    public function getServicesList()
    {
        $services = [];

        return $services;
    }

    /**
     * @param string $serviceName
     * @param string $version
     */
    public function getService($serviceName, $version = false)
    {
        $service = $this->requestService($serviceName, $version);

        return $service;
    }

    /**
     * @return ClientHTTP
     */
    protected function getClient($props = [])
    {
        if (!$this->clientHTTP) {
            $config = [
                'base_uri' => $this->getDiscoverURL(),
                'headers' => [
                    'X-AUTH-API' => $this->getProp(self::DISCOVER_AUTH_TOKEN)
                ]
            ];

            $config = array_replace_recursive($config, $props);

            $this->clientHTTP = new ClientHTTP($config);
        }

        return $this->clientHTTP ;
    }

    /**
     * @param $serviceName
     * @param $version
     * @return string
     */
    protected function buildRequestServiceURI($serviceName, $version = false)
    {
        $uri = '/v1/service/' . $serviceName;

        if ($version) {
            $uri .= '/' . $version;
        }
        return $uri;
    }

    /**
     * @param $arr
     * @param $requestCode
     * @param array $props
     * @throws ClientException
     */
    protected function verifyRequestResponse($arr, $requestCode, $props = [])
    {
        switch ($requestCode) {
            case self::REQUEST_GET_SERVICE:

                if (!is_array($arr)) {
                    throw new ClientException('Error in communication with Discovery');
                } elseif (isset($arr['success']) && !$arr['success']) {
                    throw new ClientException('Error in Service response');
                } elseif (!isset($arr['rows']) || (count($arr['rows']) == 0)) {
                    $msg = "Service {$props[serviceName]} ";
                    if ($props['version']) {
                        $msg .= " (with version={$props[version]}) ";
                    }
                    $msg .= 'not found';
                    throw new \Exception($msg);
                }
                break;
        }
    }

    /**
     * @param $serviceName
     * @param $version
     * @return ServiceBO
     * @throws ClientException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function requestService($serviceName, $version)
    {
        $uri = $this->buildRequestServiceURI($serviceName, $version);

        try {
            $response = $this->getClient()->request('options', $uri);

            $text = $response->getBody()->getContents();

            $arr = json_decode($text, true);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            if ($e->getResponse()->getStatusCode() == 404) {
                $msg = "Service $serviceName not found";
            } else {
                $msg = "Error retrieving service $serviceName ";
            }
            $newException = new ClientException($msg);
            $newException->setStatusCode($e->getResponse()->getStatusCode());
            throw $newException;
        } catch (\Exception $e) {
            $newException = new ClientException('Error in Discovery communication');
            throw $newException;
        }

        $this->verifyRequestResponse($arr, self::REQUEST_GET_SERVICE);

        $service = $arr['rows'][0];

        if (isset($service['key'])) {
            $uriGetService = $this->buildRequestServiceURI($service['key']);
            $arr = $this->getClient()->requestRaw('get', $uriGetService);

            if (is_array($arr)) {
                $obj = new ServiceBO();
                $obj->bind($arr);
                return $obj;
            }
        }

        throw new ClientException('Error in service request');
    }

    /**
     * @param $record
     */
    public function createService($record)
    {
        $this->validateService($record);

        $uriCreateService = '/v1/service';

        $response = $this->getClient()->request('post', $uriCreateService, ['json' => ['service' => $record]]);

        //print_r($response->getBody()->getContents());
        //exit;
        return json_decode($response->getBody()->getContents(), true);
    }

    protected function validateService($record = [])
    {
    }
}
