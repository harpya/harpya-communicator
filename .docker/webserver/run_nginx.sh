#!/usr/bin/env bash
export DOLLAR='$'
envsubst '\$WEBSERVER_ADDR \$WEBSERVER_PORT' < /root/default.conf.template > /etc/nginx/conf.d/default.conf
nginx -g "daemon off;"
